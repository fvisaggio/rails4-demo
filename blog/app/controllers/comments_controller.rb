class CommentsController < ApplicationController
  http_basic_authenticate_with name: "gerd", password: "gerd",  only: [:destroy]
  def create
    @article = Article.find(params[:article_id])
    @comment = Comment.new(comment_params)
    if @comment.save
      @article.comments.create(comment_params)
      redirect_to article_path(@article)
    else 
      redirect_to article_path(@article)
    end
  end
  
  def destroy 
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to article_path(@article)
  end 

  private
    def comment_params
      params.require(:comment).permit(:commenter, :body)
    end
end
