class Comment < ActiveRecord::Base
  belongs_to :article
  validates :commenter, presence: true,
                    length: { minimum: 4, maximum: 100 }
  validates :body, presence: true,
                    length: { minimum: 1, maximum: 140 }

  after_save :require_fields_to_be_present

  private 

  def require_fields_to_be_present
  	if self.body.blank?
  		errors.add(:input, "Comment cannot be blank")
      	raise ActiveRecord::Rollback
  	elsif self.commenter.blank?
  		errors.add(:input, "You need to specify the commenter")
      	raise ActiveRecord::Rollback
  	end 
  end
end
