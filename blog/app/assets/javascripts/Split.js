function Split() {
  // init
}
Split.prototype.split = function(originalString) {
  this.originalString = originalString;
  this.middleName="N/A";
  this.lastName="N/A";
  var partsOfString = originalString.split(" ");
  if (partsOfString.length == 1)
  {
    this.firstName=partsOfString[0];
  }
  else if (partsOfString.length == 2) {
    this.firstName = partsOfString[0];
    this.lastName = partsOfString[1];
  }
  else if (partsOfString.length == 3) {
    this.firstName = partsOfString[0];
    this.middleName = partsOfString[1];
    this.lastName = partsOfString[2];
  }
  else {
    throw new Error("name to be parsed has too many spaces");
  }
};

