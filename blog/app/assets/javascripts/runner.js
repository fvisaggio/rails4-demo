 $(document).ready(function() {
   splitter = new Split();

  $( ".comment" ).mouseenter(function() {
    meta_comment=$(this);
    name= $( '.commenter', $( this ) ).html ();
    comment= $( '.comment_body', meta_comment ).html ();

    splitter.split(name);

    $("#last_name").html(splitter.lastName);
    if (splitter.middleName!=undefined){
        $("#middle_name").html(splitter.middleName);
    }
    else{
       $("#middle_name").html("N/A");
    }
    $("#first_name").html(splitter.firstName);

    $(".quote").html(splitter.firstName + " said '"+comment+"'");
  });

});


