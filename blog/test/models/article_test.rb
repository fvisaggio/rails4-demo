require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
 
 test "articles can be created" do
  article = create_article_with_title("Boston Red Sox")
  assert article.id 
  assert_equal "Boston Red Sox", article.title 
 end

 test "articles can found by title " do
  create_article_with_title("Red Sox")
  create_article_with_title("Braves")
  article = create_article_with_title("Yankees")

  article_retrieved = Article.find_by_title("Yankees")
  assert_equal article, article_retrieved , "articles are not able to be found by their title"
 end

  test "article_title_must_be_present" do  
    article = Article.create
    assert !article.errors.blank?, "articles dont throw an error if the title isnt present"
  end

  test "article_titles_must_be_longer_than_5" do
  	 article = Article.create(:title => "gerd")
     assert !article.errors.blank? , "articles dont throw an error if the title is under 5 characters"
  end

  test "article gives descriptive error messages for imporper title" do 
  	  article = Article.create(:title => "The Red Sea is a seawater inlet of the Indian Ocean, lying between Africa and Asia. The connection to the ocean is in the south through the Bab el Mandeb strait and the Gulf of Aden. In the north, there is the Sinai Peninsula, the Gulf of Aqaba, and the Gulf of Suez (leading to the Suez Canal). The Red Sea is a Global 200 ecoregion. The sea is underlain by the Red Sea Rift which is part of the Great Rift Valley.The Red Sea has a surface area of roughly 438,000 km² (169,100 mi²).[1][2] It is about 2250 km (1398 mi) long and, at its widest point, 355 km (220.6 mi) wide. It has a maximum depth of 2211 m (7254 ft) in the central median trench, and an average depth of 490 m (1,608 ft). However, there are also extensive shallow shelves, noted for their marine life and corals. The sea is the habitat of over 1,000 invertebrate species, and 200 soft and hard corals. It is the world's northernmost tropical sea. ")
	assert_match "is too long" , article.errors.messages[:title][0]
     assert !article.errors.blank?, "articles dont throw an error when the title is too long"
  end


end