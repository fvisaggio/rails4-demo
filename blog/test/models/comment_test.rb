require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  test "comment must have the commenter enabled" do
   	comment = Comment.create
    assert !comment.errors.blank?
  end

  test "commenters name must be at least 4 letters" do
   	comment = Comment.create(:commenter => "bob")
    assert !comment.errors.blank?
    assert_match "blank" , comment.errors[:body].first  # title cant be blank
    assert_match "too short" , comment.errors[:body].last  # name is too short error

  end

  test "comment is saved if body of message is not blank" do
  	   	comment = Comment.create(:commenter => "ROY JONES", :body => "hello world")
  	   	assert comment.errors.blank?
  end

  test "comments can be searched for by their id" do
      comment = Comment.create(:commenter => "ROY JONES2", :body => "hello world")
      comment_retrieved = Comment.find_by_id(comment.id)
      assert_equal comment_retrieved, comment
  end

end
