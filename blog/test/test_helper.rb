ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  fixtures :all

  def create_article
  	  article = Article.create(:title => "longest yard ",:text => 'a semi true story', :id => 1)
      article.save!
      return article
  end

  def generate_comment_params
  	comment_params = { :commenter => "brett favre", :body =>"lets go for it", :id => 1}
  end

  def http_login
    	user = 'gerd'
    	pw = 'gerd'
    	request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(user,pw)
  end 

  def create_article_with_title(title)
    article = Article.create(:title => title,:text => 'a semi true story')
    article.save!
    return article
  end 

  def create_article_with_title_and_text(title , text)
  	article = Article.create(:title => title,:text => text)
    article.save!
    return article
  end
  # Add more helper methods to be used by all tests here...
end
