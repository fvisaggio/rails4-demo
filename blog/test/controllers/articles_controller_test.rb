require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase

	# expected actual
  test "articles with these options generate this path (show)" do
		assert_generates '/articles/1',
		 { controller: 'articles', action: 'show', id: '1' }
  end

  test "going to the show path will route it to the the right controller with the right options " do
  	assert_recognizes({ controller: 'articles', action: 'show', id: '1' }, '/articles/1')
  end

  test "index has the route for all articles " do
  	#  assert_routing combines the first 2 in 1 statement. 
  	# ie given the options you get the right path 
  	# given the right path it makes the right options
  	 assert_routing({ path: '/articles', method: :get },
  	  { controller: 'articles', action: 'index' })
  end

  test "articles can be destroyed route exists" do
    assert_routing({ path: '/articles/1', method: :delete },
    { controller: 'articles', action: 'destroy', id: '1' })
  end

  test "articles can be created route" do
    assert_routing({ path: '/articles', method: :post }, { controller: 'articles', action: 'create' })
  end 


  test "index returns renders index template/layout" do
    create_article_with_title_and_text("title", "text about the title")
    xhr :get, :index
    assert_response 200 
    assert_template layout: "layouts/application"
  end
  
  test "index shows all the articles and renders correctly" do 
    create_article_with_title("ny jets")
    create_article_with_title("giants")
    xhr :get, :index
    assert_match "jets", @response.body , "articles not displaying properly on index page"
    assert_match "giants", @response.body , "articles not displaying properly on index page"
    assert_template :index , "Index is not rendered when getting all articles"
  end

  test "viewing a single article renders the show page " do 
    article = create_article_with_title("ny jets")
    xhr :get, :show, {id: article.id}
    assert_template :show , "show is not rendered when viewing a single article"
  end 

 test "viewing a single article displays the text also" do 
    article = create_article_with_title_and_text("football", "packers")
    xhr :get, :show, {id: article.id}
    assert_match "packers" , @response.body, "the article text isnt being displayed"
  end 

  test "articles renders comment partial for each comment" do 
    article = create_article_with_title("ny jets")
    article.comments.create(:commenter => 'sir gerd', :body => 'Mr gerd went to the store')
    article.comments.create(:commenter => 'sir foo', :body => 'Mr foo went to the store')
    xhr :get, :show, {id: article.id}
    assert_match "gerd", @response.body , "comment is not being displayed"
    assert_match "foo", @response.body , "comment is not being displayed"
    assert_template :partial => 'comments/_comment', :count => 2 #really cool see that comment partial was rendered twice
  end

  test "delete fails without basic auth " do
  	article = create_article_with_title_and_text("title", "text about the title")
 	  xhr :delete, :destroy, {id: article.id}
    assert_response 401 #401 = unauthorized http access 
    assert Article.find_by_id(article.id)
  end

  test "delete works with username and password " do
  	article = create_article_with_title_and_text("title", "text about the title")
 	  http_login
 	  xhr :delete, :destroy, {id: article.id}
    assert_response 302 #redirect when you delete it 
    assert Article.find_by_id(article.id).nil?
  end

 test "delete article also deletes the stranded comments" do
   init_comments = Comment.all.length
   article = create_article_with_title_and_text("title", "text about the title")
   article.comments.create(:commenter => 'sir gerd', :body => '12345678')
 	 http_login
   # :dependent => :destroy in the model 
 	 xhr :delete, :destroy, {id: article.id}
     assert_equal 0, Comment.all.length-init_comments
  end

end
