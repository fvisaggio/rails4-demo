require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
 
 test "comments recognize creation route" do
  assert_routing({ path: '/articles/1/comments', method: :post }, 
    { controller: 'comments', action: 'create', article_id: '1'})
 end

 test "comments recognize the show route" do
  assert_routing({ path: '/articles/1/comments/1', method: :get }, 
        { controller: 'comments', action: 'show', article_id: '1', id: '1' })
 end

 test "comments recognize the destroy route" do
  assert_routing({ path: '/articles/1/comments/1', method: :delete }, 
        { controller: 'comments', action: 'destroy', article_id: '1', id: '1' })
 end

 test "comments can be created and will trigger a redirect" do
      article = create_article
      comment_params = generate_comment_params
      xhr :post, :create , { :comment => comment_params, :article_id => 1} 
  	  assert_response :redirect , "upon creation of a comment the redirect no longer happens"
      assert !article.comments.blank? , "the comment was not created for this article"
  end

  test "comments cannot be destroyed unless your logged in " do
      article = create_article
      comment_params = generate_comment_params
      xhr :post, :create , { :comment => comment_params, :article_id => 1} 
      xhr :delete, :destroy , { :comment => comment_params, :article_id => 1, :id => 1} 
      assert_match "Access denied", response.body.inspect
      assert !article.comments.blank? , "the comment was deleted for this article even though the user didnt log in "
  end 

  test "comments can be destroyed and will trigger a redirect " do
      article = create_article
      comment_params = generate_comment_params
      xhr :post, :create , { :comment => comment_params, :article_id => 1} 
      http_login
      xhr :delete, :destroy , {:article_id => 1, :id => 1} 
      assert_response 401 , "upon deletion of a comment the redirect no longer happens"
      assert article.comments.blank? , "the comment was not deleted for this article"
  end 

end
